import express from 'express';
var app = express();
app.use(express.json());

const port = 3000;

var books = [{ name: "The Principle", pages: 102 }, { name: "The One", pages: 202 }];
var users = [{ user: "Perico", password: "12345", id: 1 }];

app.post('/user', function (request, response) {
    if (!request.body.username || typeof request.body.username != "string" || !request.body.password || typeof request.body.password != "string") {
        response.status(400).json({ message: "username has a wrong format" });
        return;
    }
    let newId = users[users.length - 1].id + 1;
    users.push({ user: request.body.username, id: newId });
    response.json({ username: request.body.username, id: newId, message: "New user added with id " + newId });
});

app.get('/user/:id', function (request, response) {
    if (!request.params.id) {
        response.status(400).json({ message: "id not sent or has a wrong format" });
        return;
    }
    try {
        response.json({ username: users.find((user) => user.id == parseInt(request.params.id)).user, id: parseInt(request.params.id) });
    } catch (e) {
        response.status(400).json({ message: "id not sent or has a wrong format" });
        return;
    }
});

app.get('/users/all', function (request, response) {
    response.json(users).send();
});

app.post('/book', function (request, response) {
    if (!request.body.name ||
        !request.body.pages) {
        response.status(400).json('Missing some details');
        return;
    }
    if ((parseInt(request.body.pages)).toString() == "NaN" || typeof request.body.name != "string") {
        response.status(400).json('Wrong format');
        return;
    }
    books.push({ name: request.body.name, pages: parseInt(request.body.pages) });
    response.json({ name: request.body.name, pages: parseInt(request.body.pages) })
});

app.get('/book', function (request, response) {
    if (!request.query.name && !request.query.pages) {
        response.status(400).json('name and/or pages must be provided');
        return;
    }
    let auxBooks = books;
    if (request.query.name) auxBooks = auxBooks.filter((book) => book.name == request.query.name);
    if (request.query.pages) auxBooks = auxBooks.filter((book) => book.pages == request.query.pages);
    response.json(auxBooks);
});

app.get('/books/all', function (request, response) {
    response.json(books).send();
})

app.listen(port, () => {
    console.log(`API listening at http://localhost:${port}`)
});