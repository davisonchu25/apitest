#Description
This is a simple API to test againts

#Requirements
NodeJS

#How to run
First run npm install inside the project and then just run the command `node index.js` and the API will be ready to receive requests.

#Endpoints definitions
The server will run at http://localhost:3000

##user
- GET /user/:id
  `http://localhost:3000/user/1`
- GET /users/all
  `http://localhost:3000/users(all`
- POST /user
  `http://localhost:3000/user/`
  </br>JSON body:
  <pre>{
    "username": string,
    "password": string
  }</pre>

##book
- GET /book
  </br>Path parameters:
  1. name: string
  2. pages: int<br>
`http://localhost:3000/book?name=The%20Principle&&pages=102`
- GET /books/all
  `http://localhost:3000/books/all`
- POST /book
  `http://localhost:3000/book/`
JSON body:
<pre>{
"name": string,
"pages": int
}</pre>
